using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; // N�cessaire pour utiliser UI

public class CaisseCounter : MonoBehaviour
{
    public int caissesDeposees = 0; // Compteur de caisses
    public Text compteurText; // R�f�rence au texte d'affichage

    void Start()
    {
        // Initialiser le texte � 0
        UpdateCompteurText();
    }
    public void AjouterCaisse()
    {
        caissesDeposees++;
        UpdateCompteurText();
    }


    void UpdateCompteurText()
    {
        //maj du texte
        if (caissesDeposees > 0)
        {
            //Debug.Log(caissesDeposees);
            //on affoche le compteur de caisse
            //compteurText.text = "Caissse d�pos�es: ";
            compteurText.text = caissesDeposees.ToString() ;
        }
    }
}
