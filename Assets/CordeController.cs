using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CordeController : MonoBehaviour
{
    public Transform pointAncrage;
    public Transform crochet;
    Vector3 offset = new Vector3(0, 0, -0.3f);

    private LineRenderer lineRenderer;

    void Start()
    {

        lineRenderer = GetComponent<LineRenderer>(); // on recupere la corde
    }

    void Update()
    {
        Vector3 worldAnchorPosition = pointAncrage.TransformPoint(offset); // le point de depart (mouffle)

        lineRenderer.SetPosition(0, worldAnchorPosition);
        lineRenderer.SetPosition(1, crochet.position); // le point sur lequel on se base (crochet) (il faut rester entre les 2)
    }
}