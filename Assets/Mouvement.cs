using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouvement : MonoBehaviour
{
    public bool isgrounded = true;
    Rigidbody rb;
    public float gravity = 5;
    public float allure = 0.01f;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        allure = 2f;
        // on peut changer les valeurs dans les translate et dans les rotate
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(Vector3.down * allure * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.UpArrow) ) // les touches sont en qwerty on utilise ZQSD
        {
            transform.Translate(Vector3.up * allure * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.LeftArrow) )
        {
            transform.Rotate(Vector3.forward * allure * Time.deltaTime, -1f);
        }
        if (Input.GetKey(KeyCode.RightArrow) )
        {
            transform.Rotate(Vector3.forward * Time.deltaTime, 1f);
        }
        //if (Input.GetKey(KeyCode.Space) && isgrounded)
        //{
        //    //transform.Translate(Vector2.up * 1);
        //    //rb.AddForce(Vector3.up * 5, ForceMode.Impulse);
        //    //isgrounded = false;
        //}
    }

    //void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.CompareTag("Sol"))
    //    {
    //        isgrounded = true;
    //        //Debug.Log("a touch� Sol");
    //    }
    //}

}
