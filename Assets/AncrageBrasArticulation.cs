using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AncrageBrasArticulation : MonoBehaviour
{
    public GameObject Rotation;
    public float speed;
    public int RotationDirection;

    private ArticulationBody articulation;

    /**
     * C'est le m�me code que pour la rotation des premieres articulations
     * elle n'est pas utilisable car d�cale en rotation le bras sur l'axe des Z, je n'ai pas r�ussi a fix le probl�me, 
     * donc j'ai pr�f�r� gard� une version fonctionnelle sans les Articulation Body sur support_02 et support_03
     */

    void Start()
    {
        articulation = Rotation.GetComponent<ArticulationBody>();
    }

    void Update()
    {
        EtatRotation rotationState = EtatRotation.Fixe;

        if (Input.GetKey(KeyCode.K))
        {
            rotationState = EtatRotation.Positif;
        }
        else if (Input.GetKey(KeyCode.L))
        {
            rotationState = EtatRotation.Negatif;
        }

        ApplyRotationX(rotationState, articulation); // Appliquer la rotation � l'articulation principale

    }

    void ApplyRotationX(EtatRotation rotationState, ArticulationBody articulationBody)
    {
        if (rotationState != EtatRotation.Fixe)
        {
            float rotationChange = (float)rotationState * speed * Time.deltaTime;
            var drive = articulationBody.xDrive;

            if (rotationChange > 0)
            {
                drive.targetVelocity = 45 * RotationDirection;
            }
            else
            {
                drive.targetVelocity = -45 * RotationDirection;
            }

            float rotationGoal = CurrentPrimaryAxisRotation(articulationBody) + rotationChange * RotationDirection;
            RotateToX(rotationGoal, articulationBody);
        }
    }

    float CurrentPrimaryAxisRotation(ArticulationBody articulationBody)
    {
        float currentRotationRads = articulationBody.jointPosition[0];
        float currentRotation = Mathf.Rad2Deg * currentRotationRads;
        return currentRotation;
    }
    void RotateToX(float primaryAxisRotation, ArticulationBody articulationBody)
    {
        var drive = articulationBody.xDrive;
        drive.target = primaryAxisRotation;
        articulationBody.xDrive = drive;
    }
}