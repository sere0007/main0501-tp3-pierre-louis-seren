using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationBras : MonoBehaviour
{
    public ArticulationBody s1;
    public float rotationSpeed = 100f; // Vitesse de rotation
    public float maxRotationAngle = 110f; // Max de rota � changer dans l'�diteur Unity pour chaque bras (angle different si bras 02 ou bras 03)
    private float minRotationAngle = 0f; // pos initiale du bras

    // Direction de la rotation: 1 pour droite, -1 pour gauche
    public int rotationDirection = 1; // Par d�faut, rotation � droite (�ai nverse le sens de rotation dcp)

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once par frame
    void Update()
    {
        float rotaEnCours = transform.localEulerAngles.z;
        if (rotaEnCours > 180f)
            rotaEnCours -= 360f;

        if (rotationDirection == 1)
        {
            // Rotation � droite
            if (Input.GetKey(KeyCode.O) && rotaEnCours < maxRotationAngle)
            {
                transform.Rotate(Vector3.forward * rotationSpeed * Time.deltaTime);
            }
            else if (Input.GetKey(KeyCode.P) && rotaEnCours > minRotationAngle)
            {
                transform.Rotate(Vector3.forward * -rotationSpeed * Time.deltaTime);
            }
        }
        else if (rotationDirection == -1)
        {
            // Rotation � gauche
            if (Input.GetKey(KeyCode.O) && rotaEnCours > -maxRotationAngle)
            {
                transform.Rotate(Vector3.forward * -rotationSpeed * Time.deltaTime);
            }
            else if (Input.GetKey(KeyCode.P) && rotaEnCours < minRotationAngle)
            {
                transform.Rotate(Vector3.forward * rotationSpeed * Time.deltaTime);
            }
        }
    }
}