using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadlightController : MonoBehaviour
{
    public GameObject Phare1;
    public GameObject Phare2;
    private int etat = 1; // 1 = allum�, 0 = �teins


    // Start is called before the first frame update
    void Start()
    {
        Phare1.SetActive(true);
        Phare2.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        // pour chaque input 1 - 2 - 3 - 4 on desactive les camera non utilis� et on active la cam�ra utilis� 
        if (Input.GetKeyDown(KeyCode.H))
        {
            if (etat == 1)
            {
                Phare1.SetActive (false);
                Phare2.SetActive (false);
                etat = 0;
            }else
            {
                Phare1.SetActive(true);
                Phare2.SetActive(true);
                etat = 1;
            }
        }
       

    }
}
