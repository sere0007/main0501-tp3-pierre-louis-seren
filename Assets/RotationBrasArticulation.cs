using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Classe reprise de RotationCommande et RotationControleur mais modifie pour axe Z
public class RotationBrasArticulation : MonoBehaviour
{
    public GameObject Rotation;
    public float speed = 30.0f;
    public int RotationDirection;

    private ArticulationBody articulation;
    void Start()
    {
        articulation = Rotation.GetComponent<ArticulationBody>(); // on r�cup�re notre mat
    }

    void Update()
    {
        // On mets l'�tat � fixe pour �viter que le mat se balade
        EtatRotation rotationState = EtatRotation.Fixe;

        if (Input.GetKey(KeyCode.O)) // pour tourner � droite
        {
            rotationState = EtatRotation.Positif;
        }
        else if (Input.GetKey(KeyCode.P)) // pour tourner � gauche
        {
            rotationState = EtatRotation.Negatif;
        }

        ApplyRotation(rotationState);
    }

    void ApplyRotation(EtatRotation rotationState)
    {
        //on effectue une rotation si l'�tat n'est pas rest� sur fixe pour �viter de tout casser
        if (rotationState != EtatRotation.Fixe)
        {
            // vitesse en fonction du temps et pas des fps
            float rotationChange = (float)rotationState * speed * Time.deltaTime;
            var drive = articulation.zDrive;
            if (rotationChange > 0)
            {
                drive.targetVelocity = 45 * RotationDirection;
            }
            else // on inverse le sens de rotation grace � la v�locit�, et selon la direction qu'il doit prendre // rotation direction change rien
            {
                drive.targetVelocity = -45 * RotationDirection;
            }
            // rotation cible
            float rotationGoal = CurrentPrimaryAxisRotation() + rotationChange * RotationDirection;
            RotateTo(rotationGoal);
        }
    }

    float CurrentPrimaryAxisRotation()
    {
        // methode de rotationControleur
        float currentRotationRads = articulation.jointPosition[0];
        float currentRotation = Mathf.Rad2Deg * currentRotationRads;
        return currentRotation;
    }
    void RotateTo(float primaryAxisRotation)
    {
        var drive = articulation.zDrive;
        drive.target = primaryAxisRotation;
        //on mets � jour la pos
        articulation.zDrive = drive;
    }
}
