using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AfficheControles : MonoBehaviour
{
    public GameObject controles;
    public GameObject afficheControle;
    private bool actif;

    void Start()
    {
        actif = true;
        controles.SetActive(!actif);
        afficheControle.SetActive(actif);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log(actif);
            actif = !actif;
            controles.SetActive(!actif);
            afficheControle.SetActive(actif);
        }
    }
}
