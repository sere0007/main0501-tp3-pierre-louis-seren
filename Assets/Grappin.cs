using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grappin : MonoBehaviour
{
    public CaisseCounter caisseCounter;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            Destroy(this.gameObject.GetComponent<FixedJoint>());
        }
    }

    void OnCollisionEnter(Collision Collision)
    {

        if (Collision.gameObject.CompareTag("crochet"))
        {
            if (Collision.gameObject.GetComponent<ArticulationBody>() != null)
            {
                FixedJoint joint = this.gameObject.AddComponent<FixedJoint>(); joint.connectedArticulationBody = Collision.articulationBody;
            }
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        // V�rifie si la caisse entre dans le d�p�t
        if (other.CompareTag("Depot")) // tag de la plateforme de depot
        {
            //Debug.Log("Touch� !");
            // Ajoute une caisse au compteur
            caisseCounter.AjouterCaisse();
        }
    }
}
