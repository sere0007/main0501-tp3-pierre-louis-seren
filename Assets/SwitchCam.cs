using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCam : MonoBehaviour
{
    public GameObject MainCamera;
    public GameObject MoufleCam;
    public GameObject FirstPersonCam;
    public GameObject CrochetCam;


    // Start is called before the first frame update
    void Start()
    {
        MainCamera.SetActive(true);
        MoufleCam.SetActive(false);
        FirstPersonCam.SetActive(false);
        CrochetCam.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        // pour chaque input 1 - 2 - 3 - 4 on desactive les camera non utilis� et on active la cam�ra utilis� 
        if (Input.GetKey(KeyCode.Alpha1))
        {
            MainCamera.SetActive(true);
            MoufleCam.SetActive(false);
            FirstPersonCam.SetActive(false);
            CrochetCam.SetActive(false);
        }
        if (Input.GetKey(KeyCode.Alpha2))
        {
            MainCamera.SetActive(false);
            MoufleCam.SetActive(true);
            FirstPersonCam.SetActive(false);
            CrochetCam.SetActive(false);
        }
        if (Input.GetKey(KeyCode.Alpha3))
        {
            MainCamera.SetActive(false);
            FirstPersonCam.SetActive(true);
            MoufleCam.SetActive(false);
            CrochetCam.SetActive(false);
        }
        if(Input.GetKey(KeyCode.Alpha4))
        {
            MainCamera.SetActive(false);
            FirstPersonCam.SetActive(false);
            MoufleCam.SetActive(false);
            CrochetCam.SetActive(true);
        }

    }
}
