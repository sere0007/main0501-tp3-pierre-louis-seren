using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// D�finir l'�num�ration pour l'�tat de translation

public class TranslationCrochet : MonoBehaviour
{
    public GameObject Crochet;
    public float speed = 30.0f;

    private ArticulationBody articulation;

    void Start()
    {
        articulation = Crochet.GetComponent<ArticulationBody>();

        var drive = articulation.zDrive;
        articulation.zDrive = drive;

    }

    void Update()
    {
        EtatTranslation translationState = EtatTranslation.Fixe;

        if (Input.GetKey(KeyCode.LeftControl)) // pour descendre
        {
            //Debug.Log("Descendre");
            translationState = EtatTranslation.Negatif;
        }
        else if (Input.GetKey(KeyCode.LeftShift)) // pour monter
        {
            //Debug.Log("Monter");
            translationState = EtatTranslation.Positif;
        }
        ApplyTranslation(translationState);
    }

    void ApplyTranslation(EtatTranslation translationState)
    {
        if (translationState != EtatTranslation.Fixe)
        {
            float translationChange = (float)translationState * speed * Time.fixedDeltaTime; // Utiliser Time.fixedDeltaTime pour tps au lieu de fps
            float translationGoal = CurrentPrimaryAxisPosition() + translationChange;
            TranslateTo(translationGoal);
        }
    }

    float CurrentPrimaryAxisPosition()
    {
        // Obtenir la position actuelle sur l'axe Z
        float currentPosition = articulation.jointPosition[0];
        return currentPosition;
    }

    void TranslateTo(float primaryAxisPosition)
    {
        var drive = articulation.zDrive; // on utilise le zdrive de l'articulation

        float clampedTarget = Mathf.Clamp(primaryAxisPosition, drive.lowerLimit, drive.upperLimit);

        drive.target = clampedTarget; // Appliquer la cibble sur le drive
        articulation.zDrive = drive;
    }
}
