using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AncrageBras : MonoBehaviour
{
    public float rotationSpeed = 100f; // Vitesse de rotation
    public float maxRotationAngle = 0f; // Max de rota � changer dans l'�diteur Unity pour chaque bras
    public float minRotationAngle = 0f; // Position initiale (55� pour le 3e bras)

    // Direction de la rotation: 1 pour les bras av, -1 pour les bras arriere (�a inverse le sens de rotation)
    public int rotationDirection = 1; // Par d�faut, rotation � droite

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once par frame
    void Update()
    {
        float rotaEnCours = transform.localEulerAngles.x;
        if (rotaEnCours > 180f)
            rotaEnCours -= 360f;

        if (rotationDirection == 1)
        {
            // Rotation � droite
            if (Input.GetKey(KeyCode.K) && rotaEnCours < maxRotationAngle)
            {
                transform.Rotate(Vector3.right * rotationSpeed * Time.deltaTime);
            }
            else if (Input.GetKey(KeyCode.L) && rotaEnCours > minRotationAngle)
            {
                transform.Rotate(Vector3.right * -rotationSpeed * Time.deltaTime);
            }
        }
        else if (rotationDirection == -1)
        {
            // Rotation � gauche
            if (Input.GetKey(KeyCode.K) && rotaEnCours > -maxRotationAngle)
            {
                transform.Rotate(Vector3.right * -rotationSpeed * Time.deltaTime);
            }
            else if (Input.GetKey(KeyCode.L) && rotaEnCours < minRotationAngle)
            {
                transform.Rotate(Vector3.right * rotationSpeed * Time.deltaTime);
            }
        }
    }
}